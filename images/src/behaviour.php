<?php

/**
 * Fattura24.com
 * Description: define default behaviour, to be extended by specialized needs
 * Author: Ing. Carlo Capelli
 */

namespace fattura24;

if (!defined('ABSPATH')) exit;

require_once 'uty.php';
require_once 'constants.php';

if (!is_admin())
    require_once ABSPATH.'wp-admin/includes/file.php';
    
add_filter(DOC_PDF_FILENAME, function($args) {
    $doc_type = $args['doc_type'];
    $order_id = $args['order_id'];
    $timestamp = now('YmdHis');
    return sprintf('doc-%s-%s-%s.pdf', $timestamp, $doc_type, $order_id);
});

// access the order metadata
add_filter(ORDER_GET_VAT, function($order) {
    return get_post_meta($order->get_id(), '_billing_vatcode', true);
});
function order_p_iva($order) {
    return apply_filters(ORDER_GET_VAT, $order);
}

add_filter(ORDER_GET_CF, function($order) {
    return get_post_meta($order->get_id(), '_billing_fiscalcode', true);
});
function order_c_fis($order) {
    return apply_filters(ORDER_GET_CF, $order);
}

function customer_use_vat() {
    return apply_filters(CUSTOMER_USE_VAT, null);
}
add_filter(CUSTOMER_USE_VAT, function() { return true; });

function customer_use_cf() {
    return apply_filters(CUSTOMER_USE_CF, null);
}
add_filter(CUSTOMER_USE_CF, function() { return true; });

//new kd code
add_filter(ORDER_GET_PEC_ADDRESS, function($order) {
    return get_post_meta($order->get_id(), '_billing_pecaddress', true);
});
function order_pec_address($order) {
    return apply_filters(ORDER_GET_PEC_ADDRESS, $order);
}
add_filter(ORDER_GET_RECIPIENTCODE, function($order) {
    return get_post_meta($order->get_id(), '_billing_recipientcode', true);
});
function order_recipientcode($order) {
    return apply_filters(ORDER_GET_RECIPIENTCODE, $order);
}
function customer_use_recipientcode() {
    return apply_filters(CUSTOMER_USE_RECIPIENTCODE, null);
}
add_filter(CUSTOMER_USE_RECIPIENTCODE, function() { return true; });

function order_pecaddress($order) {
    return apply_filters(ORDER_GET_PEC_ADDRESS, $order);
}
function customer_use_pecaddress() {
    return apply_filters(CUSTOMER_USE_PEC_ADDRESS, null);
}
add_filter(CUSTOMER_USE_PEC_ADDRESS, function() { return true; });

function get_url_from_file($file) {
    $hp = get_home_path();
    $last_hp_ = explode('/',substr($hp,0,-1));
    $last_hp = end($last_hp_);
    $last_file_ = explode($last_hp,$file);
    $last_file = end($last_file_);
    return home_url().$last_file;
}

// document related locations
function PDF_filename($doc_type, $order_id) {
    /*
    $timestamp = now('YmdHis');
    return sprintf('doc-%s-%s-%s.pdf', $timestamp, $doc_type, $order_id);
    */
    return apply_filters(DOC_PDF_FILENAME, compact('doc_type', 'order_id'));
}

function set_file_permissions($new_file) {
    // Set correct file permissions
    $stat = @stat(dirname($new_file));
    $perms = $stat['mode'] & 0007777;
    $perms = $perms & 0000666;
    @chmod($new_file, $perms);
}

add_filter(DOC_STORE_FILE, function($status, $orderId, $PDF) {
    
    $docType = peek($status, 'docType', DT_FATTURA);
    $oldPdfPath = peek($status, 'pdfPath');

    $file = PDF_filename($docType, $orderId);

    /*
    if ($folder = get_option(OPT_DOCS_FOLDER))
        $folder = trailingslashit($folder);
    else
        $folder = 'fattura24/';
    */
    $folder = trailingslashit(DOCS_FOLDER);
    
    $wpdir = wp_upload_dir();
    $basedir = $wpdir['basedir'];

    $dir = $basedir.'/'.$folder;
    //if (!file_exists($dir))
        wp_mkdir_p($dir);
    
    $new_file = $dir.$file;

    if (!file_exists($dir . 'index.php'))
        file_put_contents($dir . 'index.php', '<?php');
    
    $ifp = @fopen($new_file, 'wb');
    if (!$ifp)
        order_status_set_error($status, sprintf(__('Could not write file %s', 'fatt-24'), $new_file));
    else {
        @fwrite($ifp, $PDF);
        fclose($ifp);
        set_file_permissions($new_file);
        order_status_set_file_data($status, $new_file, $docType);

        if ($oldPdfPath && is_file($oldPdfPath))
            unlink($oldPdfPath);
    }

    return $status;
}, 10, 3);

function doc_footnotes($order) {
    $FootNotes = sprintf(__('order num. %d', 'fatt-24'), $order->get_id());
    if ($order->get_customer_order_notes())
        $FootNotes .= ' - ' . $order->get_customer_order_notes();
    return $FootNotes;
}
add_filter(DOC_FOOTNOTES, function($order) {
    return doc_footnotes($order);
});

add_filter(DOC_PRODUCT_CODE, function($item) {
    $product = $item->get_product();
    return $product->get_sku();
});

add_filter(DOC_ADDRESS, function($order) {
    return make_strings(    array($order->get_billing_address_1(), $order->get_billing_address_2()),
                            array($order->get_shipping_address_1(), $order->get_shipping_address_2()));
});

add_filter(LAYOUT_OPTION, function($args) {
    extract($args);
    $rc = $widget;
    if ($help) $rc .= helpico($help);
    if ($desc) $rc .= $desc;
    return $rc;
});

/*
 * run once: move documents to fixed path folder, *outside* fattura24 folder
 * update postmeta records with new path
 */
function move_docs($folder) {
    //trace('move_docs', $folder);
    $folder = trailingslashit($folder);

    $wpdir = wp_upload_dir();
    $basedir = $wpdir['basedir'];

    $dir = $basedir.'/'.$folder;
    wp_mkdir_p($dir);
    
    $recs_postmeta = 0;
    $ok_meta_value = 0;
    $ko_meta_value = 0;
    $has_pdfPath = 0;
    $no_pdfPath = 0;
    $moved = 0;
    $cant_move = 0;
    
    global $wpdb;
    foreach($wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE meta_key=%s", ORDER_INVOICE_STATUS)) as $r) {
        ++$recs_postmeta;
        if ($i = maybe_unserialize($r->meta_value)) {
            ++$ok_meta_value;
            if (isset($i['pdfPath']) && is_file($i['pdfPath'])) {
                ++$has_pdfPath;
                $old = $i['pdfPath'];
                $file = basename($old);
                $new = $dir.$file;
                if ($new != $old && rename($old, $new)) {
                    set_file_permissions($new);
                    ++$moved;
                    $i['pdfPath'] = $new;
                    store_order_status($r->post_id, $i);
                    //store_order_status(get_the_ID(), $i)
                }
                else
                    ++$cant_move;
            }
            else
                ++$no_pdfPath;
        }
        else
            ++$ko_meta_value;
    }

    return compact(
        'recs_postmeta',
        'ok_meta_value',
        'ko_meta_value',
        'has_pdfPath',
        'no_pdfPath',
        'moved',
        'cant_move'
    );
}