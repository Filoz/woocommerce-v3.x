<?php

/**
 * Fattura24.com
 * Description: handle Fattura 24 API calls
 * Author: Ing. Carlo Capelli
 */

namespace fattura24;

if (!defined('ABSPATH')) exit;

require_once 'uty.php';
require_once 'constants.php';
require_once 'curlDownload.php';

function get_flag($key)
{
    $val = get_option($key, false);
    $ret = $val ? true : false;
    return $ret;
}

function get_flag_lit($key)
{
    return get_flag($key) ? 'true' : 'false';
}

function api_call($command, $send_data)
{
    //trace('api_call', $command, $send_data);
    $url = implode('/', array(API_ROOT, $command));
    if (!isset($send_data['apiKey']))
        $send_data['apiKey'] = get_option(OPT_API_KEY);
    $response = curlDownload($url, http_build_query($send_data));
    return $response;
}

function testApiKey()
{
    $fattura24_api_url = 'https://www.app.fattura24.com/api/v0.3/TestKey';
    $send_data = array();
    $send_data['apiKey'] = get_option(OPT_API_KEY);
    $dataReturned = curlDownload($fattura24_api_url, http_build_query($send_data));
    $xml = simplexml_load_string(str_replace('&egrave;', 'è', $dataReturned));
    $subscriptionTypeIsValid = true;
    $subscriptionDaysToExpiration = 365;
    if(is_object($xml))
    {
        $returnCode = intval($xml->returnCode);
        $description = strval($xml->description);
        if($returnCode == 1)
        {
            $subscriptionType = intval($xml->subscription->type);
            if($subscriptionType == 5 || $subscriptionType == 6)
                $subscriptionTypeIsValid = true;
            else
                $subscriptionTypeIsValid = false;
            $subscriptionExpire = strval($xml->subscription->expire);
            $date1 = now();
            $date2 = str_replace('/', '-', $subscriptionExpire);
            $diff = abs(strtotime($date1) - strtotime($date2));
            $subscriptionDaysToExpiration = ceil($diff / 86400);                
        }
    }
    else
    {
        $returnCode = '?';
        $description = 'Errore generico, per favore contatta il nostro servizio tecnico a info@fattura24.com';
    }
    return array(
        'returnCode' => $returnCode,
        'description' => $description,
        'subscriptionTypeIsValid' => $subscriptionTypeIsValid,
        'subscriptionDaysToExpiration' =>  $subscriptionDaysToExpiration
    );
}

/*
 * issue the appropriate API calls
 */
function store_fattura24_doc($orderId, $docType)
{
    $xml = order_to_XML($orderId, $docType);
    $res = api_call('SaveDocument', array('xml' => $xml));
    trace('Store Fattura24 Doc', $orderId, $docType, $xml, $res);

    $status = get_order_status($orderId);
    $ans = simplexml_load_string($res);

    if (is_object($ans))
    {
        order_status_set_doc_data($status, intval($ans->returnCode), strval($ans->description), intval($ans->docId), $docType);
        $rc = true;
    }
    else
    {
        order_status_set_error($status,
            sprintf(__('Unknow error occurred while uploading order %d', 'fatt-24'), $orderId));
        $rc = false;
    }
    store_order_status($orderId, $status);
    return $rc;
}

function download_PDF($orderId)
{

    $docId = is_available_on_f24($orderId, DT_FATTURA);
    //trace('Download PDF', $orderId, $docId);
    $rc = false;
    if ($docId)
    {
        $PDF = api_call('GetFile', array('docId' => $docId));
        if (substr($PDF, 0, 4) == '%PDF')
        {
            $status = apply_filters(DOC_STORE_FILE, $status, $orderId, $PDF);
            $rc = true;
        }
        else
        {
            $ans = simplexml_load_string($PDF);
            if ($ans)
                order_status_set_error($status, !is_object($ans) ? error_get_last() : strval($ans->description));
            else
                order_status_set_error($status, sprintf(__('Unknow error occurred while downloading PDF for order %d', 'fatt-24'), $orderId));
        }
        store_order_status($orderId, $status);
    }
    return $rc;
}

function vat_from_price_and_tax($PriceWithout, $VatPayed)
{
    $Total = $PriceWithout + $VatPayed;
    if ($PriceWithout != 0)
    {    
        $Perc = ($Total - $PriceWithout) * 100.0 / $PriceWithout;
        return round($Perc, 2);
    }
    return 0;
}

function price_without_vat($Price, $VatPerc)
{
    $X = 100.0 / $VatPerc;
    $Y = $X + 1;
    $Parz = $Price / $Y;
    return $Price - $Parz;
}

function percentage($full, $discounted)
{
    if ($full > 0)
        return 100.0 * (1.0 - $discounted / $full);
    return 0;
}

function make_strings($v1, $v2)
{
    $s = trim(implode(' ', $v1));
    if ($s == '')
        $s = trim(implode(' ', $v2));
    return $s;
}

/*
 * translate an actual order to XML format as required by Fattura24
 */
function order_to_XML($order_id, $docType)
{

    $order = new \WC_Order($order_id);
    //trace('o order', $order);
    $DATAMODIFICA = now();
    $user = $order->get_user();
    $couponApplied = false;
    $couponDiscounts = array();
    $taxTotals = array();
    $taxNames = array();

    $fixnum = function ($n, $p) {
        $n = round($n, $p);
        return sprintf("%0.{$p}f", $n);
    };


    $billing_address = $order->get_formatted_billing_address();
    $shipping_address = $order->get_formatted_shipping_address();

    $Email = $order->get_billing_email();
    if (!$Email)
        if ($user)
            $Email = $user->user_email;

    $CellPhone = $order->get_billing_phone();
    $Address = apply_filters(DOC_ADDRESS, $order);
    $Postcode = make_strings(array($order->get_billing_postcode()),
        array($order->get_shipping_postcode()));
    $City = make_strings(array($order->get_billing_city()),
        array($order->get_shipping_city()));
    $Province = make_strings(array($order->get_billing_state()),
        array($order->get_shipping_state()));
    $Country = WC()->countries->countries[make_strings(array($order->get_billing_country()),
        array($order->get_shipping_country()))];

    $FiscalCode = order_c_fis($order);
    $VatCode = order_p_iva($order);

    //Caygari Code 
	$Recipientcode = order_recipientcode($order);
    $Pecaddress = order_pecaddress($order);
    $Pecaddress = (!empty($Pecaddress) ? $Pecaddress : '0000000');

    $Name = '';
    if ($VatCode)
        $Name = make_strings(array($order->get_billing_company()),
            array($order->get_shipping_company()));
    if (empty($Name))
        $Name = make_strings(array($order->get_billing_first_name(), $order->get_billing_last_name()),
            array($order->get_shipping_first_name(), $order->get_shipping_last_name()));

    $xml = new \XMLWriter();
    if (!$xml->openMemory())
        throw new \Exception(__('Cannot openMemory', 'fatt-24'));

    $xml->startDocument('1.0', 'UTF-8');
    $xml->setIndent(2);
    $xml->startElement('Fattura24');
    $xml->startElement('Document');

    $field = function ($v, $max) {
        return substr($v, 0, $max);
    };
    $customerData = array(
        'Name' => $Name,
        'Address' => $field($Address, API_FIELD_MAX_indirizzo),
        'Postcode' => $field($Postcode, API_FIELD_MAX_cap),
        'City' => $field($City, API_FIELD_MAX_citta),
        'Province' => $field($Province, API_FIELD_MAX_provincia),
        //'Country' => $field($Country, API_FIELD_MAX_paese),
        'Country' => $order->get_shipping_country(),
        'CellPhone' => $CellPhone,
        'FiscalCode' => $FiscalCode,
        'VatCode' => $VatCode,
        'Email' => $Email,
        'Recipientcode' => $Recipientcode,
        'PEC' => $Pecaddress,

    );
    $customerData = apply_filters(CUSTOMER_USER_DATA, $customerData);

    $DeliveryName = $order->get_shipping_company();
    if (empty($DeliveryName))
        $DeliveryName = trim($order->get_shipping_first_name() . " " . $order->get_shipping_last_name());
    $DeliveryAddress = trim(trim($order->get_shipping_address_1()) . " " . trim($order->get_shipping_address_2()));
    $DeliveryPostcode = $order->get_shipping_postcode();
    $DeliveryCity = $order->get_shipping_city();
    $DeliveryProvince = $order->get_shipping_state();
    $DeliveryCountry = $order->get_shipping_country();

    $customerDeliveryData = array(
        'Name' => $DeliveryName,
        'Address' => $DeliveryAddress,
        'Postcode' => $DeliveryPostcode,
        'City' => $DeliveryCity,
        'Province' => $DeliveryProvince,
        'Country' => $DeliveryCountry,
    );

    if ($docType == DT_ORDINE) {
        $DocumentType = DT_ORDINE;
        $SendEmail = get_flag_lit(ORD_SEND);
        $updateStorage = get_option(ORD_STOCK);
        if (empty($customerDeliveryData['Address']))
            $template = get_option(ORD_TEMPLATE);
        else
            $template = get_option(ORD_TEMPLATE_DEST);
    } else {
        $xml->writeElement('Object', 'Ordine WooCommerce N. ' . $order_id);

        if (get_flag_lit(INV_DISABLE_RECEIPTS) == "true")
            $DocumentType = DT_FATTURA_FORCED;
        else
            $DocumentType = $VatCode ? DT_FATTURA : DT_RICEVUTA;
        if ($DocumentType == DT_RICEVUTA)
            $numerator = get_option(INV_SEZIONALE_RICEVUTA);
        else
            $numerator = get_option(INV_SEZIONALE_FATTURA);
        if ($numerator !== 'Predefinito') {
            $idNumerator = rtrim(end(explode(' (ID: ', $numerator)), ')');
            $xml->writeElement('IdNumerator', $idNumerator);
        }
        $SendEmail = get_flag_lit(INV_SEND);
        $updateStorage = get_option(INV_STOCK);
        if (empty($customerDeliveryData['Address']))
            $template = get_option(INV_TEMPLATE);
        else
            $template = get_option(INV_TEMPLATE_DEST);
        $xml->writeElement('F24OrderId', is_available_on_f24($order_id, DT_ORDINE));
    }
	
	//Caygari Code 
	$fatt_inv_create_new = get_option("fatt-24-inv-create");
	if($fatt_inv_create_new == 2){
		$DocumentType = "FE";
	}
	//Endcaygari Code
	
    $xml->writeElement('DocumentType', $DocumentType);
    $xml->writeElement('SendEmail', $SendEmail);
    $xml->writeElement('UpdateStorage', $updateStorage);
    if ($template !== 'Predefinito') {
        $idTemplate = rtrim(end(explode(' (ID: ', $template)), ')');
        $xml->writeElement('IdTemplate', $idTemplate);
    }
	
    foreach ($customerData as $k => $v)
        $xml->writeElement('Customer' . $k, $v);
		

    foreach ($customerDeliveryData as $k => $v)
        if (!empty($v))
            $xml->writeElement('Delivery' . $k, $v);
			
    $payment_method = $order->get_payment_method();
    $payment_method_title = $order->get_payment_method_title();
	
	//Caygari Code
	$fatt_inv_create_new = get_option("fatt-24-inv-create");
	if($fatt_inv_create_new == 2 && $DeliveryCountry == 'IT'){
		$xml->writeElement('FeCustomerPec', $Pecaddress);
		$xml->writeElement('FeDestinationCode', $Recipientcode);
	}
	if ($payment_method == 'bacs'){
		$fepaymentcode = 'MP05';
		if(!empty($payment_method_title)){
			$PaymentMethodName = $payment_method_title. '- '.$payment_method;
		} else {
			$PaymentMethodName = $payment_method;
		}
		$xml->writeElement('FePaymentCode', $fepaymentcode);
		$xml->writeElement('PaymentMethodName', $PaymentMethodName);
		$bacs_accounts1 = get_option( 'woocommerce_bacs_accounts' );
        if ( !empty( $bacs_accounts1 ) ) { 
          	foreach ( $bacs_accounts1 as $bacs_account1 ) {
				$xml->writeElement('PaymentMethodDescription', $bacs_account1['iban']);
			}
        }
	} elseif($payment_method == 'ppec_paypal'){
		$fepaymentcode = 'MP08';
		if(!empty($payment_method_title)){
			$PaymentMethodName = $payment_method_title. '- '.$payment_method;
		} else {
			$PaymentMethodName = $payment_method;
		}
		$xml->writeElement('FePaymentCode', $fepaymentcode);
		$xml->writeElement('PaymentMethodName', $PaymentMethodName);
	} else {
		$fepaymentcode = 'MP01';
		if(!empty($payment_method_title)){
			$PaymentMethodName = $payment_method_title. '- '.$payment_method;
		} else {
			$PaymentMethodName = $payment_method;
		}
		$xml->writeElement('FePaymentCode', $fepaymentcode);
		$xml->writeElement('PaymentMethodName', $PaymentMethodName);
	}
	//Endcaygari Code
	
    if ($docType == DT_ORDINE)
        $xml->writeElement('Number', $order->get_order_number());

    $order_items = function($order)
    {
        $items = array();
        foreach ($order->get_items('line_item') as $item)
            $items[] = $item;
        foreach ($order->get_items('shipping') as $item)
            $items[] = $item;
        foreach ($order->get_items('fee') as $item)
            $items[] = $item;
        return $items;
    };
    
    $TotalWithoutTax = 0;
    $VatAmount = 0;
    $Total = 0;

    foreach ($order_items($order) as $item)
    {
        if($item->get_total() != 0)
        {
            $vat = vat_from_price_and_tax($item->get_total(), $item->get_total_tax());
            $stringVatCode = trim(' ' . $vat);
            $total_tax = $item->get_total_tax();
            if(!array_key_exists($stringVatCode, $taxTotals))
                $taxTotals[$stringVatCode] = $total_tax;
            else
                $taxTotals[$stringVatCode] += $total_tax;
        }

        $TotalWithoutTax += $item->get_total();
        $Total += $item->get_total() + $item->get_total_tax();
        $VatAmount += $item->get_total_tax();
    }
    
    $xml->writeElement('TotalWithoutTax', $fixnum($TotalWithoutTax, 2));
    $xml->writeElement('VatAmount', $fixnum($VatAmount, 2));
    $xml->writeElement('Total', $fixnum($Total, 2));

    $FootNotes = apply_filters(DOC_FOOTNOTES, $order);
    $xml->writeElement('FootNotes', $FootNotes);

    if ($docType != DT_ORDINE)
    {
        $xml->startElement('Payments');
        $xml->startElement('Payment');
        $xml->writeElement('Date', now('Y-m-d'));

        $xml->writeElement('Amount', $fixnum($Total, 2));
        $xml->writeElement('Paid', get_flag_lit(INV_WHEN_PAYED));
        $xml->endElement(); // Payment
        $xml->endElement(); // Payments
    }
    $xml->startElement('Rows');
    get_flag_lit(INV_CREATE);

    if(get_flag_lit(INV_CREATE) == "true")
    {
        $pdc = get_option(INV_PDC);
        if(!empty($pdc) && $pdc != 'Nessun Pdc')
            $idPdc=rtrim(end(explode(' (ID: ', $pdc)), ')');
    }
    foreach ($order_items($order) as $item)
    {
        trace('i item', $item);

        $xml->startElement('Row');

        $vatDescription='';
		$vatNature='';
        $vat = vat_from_price_and_tax($item->get_total(), $item->get_total_tax());
        foreach($order->get_items( 'tax' ) as $tax)
        {
            $orderTaxTotal = $tax->get_data()['tax_total'] + $tax->get_data()['shipping_tax_total'];
            if($fixnum($orderTaxTotal, 2) == $fixnum($taxTotals[trim(' ' . $vat)], 2))
            {
                $vatDescription = $tax->get_data()['label'];
				$vatNature = getNatureColumn($tax->get_data()['rate_id']);
                $taxNames[trim(' ' . $vat)] = $vatDescription;
            }
        }

        if ($item->get_type() == 'fee' || $item->get_type() == 'shipping')
        {
            $xml->writeElement('Description', $item->get_name());
            $xml->writeElement('Qty', 1);
            $xml->writeElement('Price', $fixnum($item->get_total(), 2));
            $xml->writeElement('VatCode', $fixnum($vat, 2));
            if($vatDescription != '')
                $xml->writeElement('VatDescription', $vatDescription);
			if($vatNature != '')
				 $xml->writeElement('FeVatNature', $vatNature);
        }
        else if ($item->get_type() == 'line_item')
        {
            $qty = $item->get_quantity();
            $price = $fixnum($item->get_subtotal() / $qty, 4);
            $xml->writeElement('Description', $item->get_name());
            $xml->writeElement('Qty', $qty);
            $xml->writeElement('Um', PRODUCT_XML_UM);
            $xml->writeElement('Price', $price);
            $xml->writeElement('VatCode', $fixnum($vat, 2));
            if($vatDescription != '')
                $xml->writeElement('VatDescription', $vatDescription);
			if($vatNature != '')
				 $xml->writeElement('FeVatNature', $vatNature);
            $Code = apply_filters(DOC_PRODUCT_CODE, $item);
            $xml->writeElement('Code', $Code);
            
            $discount = $item->get_subtotal() - $item->get_total();
            if($discount > 0)
            {
                $couponApplied = true;
                $stringVatCode = trim(' ' . $vat);
                if(!array_key_exists($stringVatCode, $couponDiscounts))
                    $couponDiscounts[$stringVatCode] = $discount;
                else
                    $couponDiscounts[$stringVatCode] += $discount;
            }
        }
        if(!empty($idPdc))
            $xml->writeElement('IdPdc', $idPdc);

        $xml->endElement(); // Row
    }
    
    if($couponApplied == true)
    {
		//Caygri code
		$vatNature='';
		foreach($order->get_items( 'tax' ) as $tax)
        {
            $orderTaxTotal = $tax->get_data()['tax_total'] + $tax->get_data()['shipping_tax_total'];
            if($fixnum($orderTaxTotal, 2) == $fixnum($taxTotals[trim(' ' . $vat)], 2))
            {
				$vatNature = getNatureColumn($tax->get_data()['rate_id']);
            }
        }
		//end caygri code
		
        $coupons = $order->get_used_coupons();
        $nCoupons = count($coupons);
        if($nCoupons == 1)
            $descrizioneCoupon = 'Coupon ' . $coupons[0];
        else if($nCoupons > 1)
        {
            $descrizioneCoupon = 'Coupons ';
            for($i=0;$i<$nCoupons;$i++)
            {
                $descrizioneCoupon .= $coupons[$i];
                if($i<$nCoupons-1)
                {
                    $descrizioneCoupon .= ', ';
                }
            }
        }
        else
            $descrizioneCoupon = 'Sconto';
        
        foreach ($couponDiscounts as $vatCode => $amount)
        {
            $price = - $amount;
            $description = $descrizioneCoupon;
            if(count($couponDiscounts) > 1)
                $description .= ' (Iva ' . $vatCode . '%)';
            
            $vatDescription = $taxNames[$vatCode];

            $xml->startElement('Row');
            $xml->writeElement('Description', $description);
            $xml->writeElement('Qty', '1');
            $xml->writeElement('Price', $fixnum($price,2));
            $xml->writeElement('VatCode', $fixnum($vatCode,2));
            if(!empty($vatDescription))
                $xml->writeElement('VatDescription', $vatDescription);
			if($vatNature != '')
				 $xml->writeElement('FeVatNature', $vatNature);
            $xml->endElement(); // end Row
        }
    }
                               
    $xml->endElement(); // Rows
    $xml->endElement(); // Document
    $xml->endElement(); // Fattura24
    $xml->endDocument();

    return $xml->outputMemory(TRUE);
}

/*
 * save Customer data in Address Book
 */
function SaveCustomer($orderId)
{
    $order = new \WC_Order($orderId);
    trace('SaveCustomer', $order);
    $xml = new \XMLWriter();
    if (!$xml->openMemory())
        throw new \Exception(__('Cannot openMemory', 'fatt-24'));
    $xml->startDocument('1.0', 'UTF-8');
    $xml->setIndent(2);
    $xml->startElement('Fattura24');
    $xml->startElement('Document');
    $user = $order->get_user();

    $billing_address = $order->get_formatted_billing_address();
    $shipping_address = $order->get_formatted_shipping_address();

    $Email = $order->get_billing_email();
    if (!$Email)
        if ($user)
            $Email = $user->user_email;

    $CellPhone = $order->get_billing_phone();
    $Address    = apply_filters(DOC_ADDRESS, $order);
    $Postcode   = make_strings( array($order->get_billing_postcode()),
                                array($order->get_shipping_postcode()));
    $City       = make_strings( array($order->get_billing_city()),
                                array($order->get_shipping_city()));
    $Province   = make_strings( array($order->get_billing_state()),
                                array($order->get_shipping_state()));
    $Country    = WC()->countries->countries[make_strings( array($order->get_billing_country()),
                                                           array($order->get_shipping_country()))];
    
    $FiscalCode = order_c_fis($order);
    $VatCode    = order_p_iva($order);

    //KD
    $Recipientcode  = order_recipientcode($order);
    $Pecaddress     = order_pecaddress($order);

    if ($VatCode)
        $Name   = make_strings( array($order->get_billing_company()),
                                array($order->get_shipping_company()));
    if (empty($Name)) // else
        $Name   = make_strings( array($order->get_billing_first_name(), $order->get_billing_last_name()),
                                array($order->get_shipping_first_name(), $order->get_shipping_last_name()));

    $field = function($v, $max) { return substr($v, 0, $max); };
    $customerData = array(
        'Name'      => $Name,
        'Address'   => $field($Address, API_FIELD_MAX_indirizzo),
        'Postcode'  => $field($Postcode, API_FIELD_MAX_cap),
        'City'      => $field($City, API_FIELD_MAX_citta),
        'Province'  => $field($Province, API_FIELD_MAX_provincia),
        'Country'   => $field($Country, API_FIELD_MAX_paese),
        'CellPhone' => $CellPhone,
        'FiscalCode'=> $FiscalCode,
        'VatCode'   => $VatCode,
        'Email'     => $Email,

    );
    $customerData = apply_filters(CUSTOMER_USER_DATA, $customerData);
    
    foreach($customerData as $k => $v)
        $xml->writeElement('Customer'.$k, $v);
    
    $DeliveryName = $order->get_shipping_company();
    if (empty($DeliveryName))
    	$DeliveryName = trim($order->get_shipping_first_name() . " " . $order->get_shipping_last_name());
    $DeliveryAddress = trim(trim($order->get_shipping_address_1()) . " " . trim($order->get_shipping_address_2()));
    $DeliveryPostcode = $order->get_shipping_postcode();
    $DeliveryCity = $order->get_shipping_city();
    $DeliveryProvince = $order->get_shipping_state();
    $DeliveryCountry = $order->get_shipping_country();
    
    $customerDeliveryData = array(
    	'Name'      => $DeliveryName,
    	'Address'   => $DeliveryAddress,
    	'Postcode'  => $DeliveryPostcode,
    	'City'      => $DeliveryCity,
    	'Province'  => $DeliveryProvince,
    	'Country'   => $DeliveryCountry,
    );
    
    foreach($customerDeliveryData as $k => $v)
    	$xml->writeElement('Delivery'.$k, $v);
    
    $xml->endElement(); // Document
    $xml->endElement(); // Fattura24
    $xml->endDocument();

    $res = api_call('SaveCustomer', array('xml' => $xml->outputMemory(TRUE)));
    $ans = simplexml_load_string($res);
    if (is_object($ans))
    {
        //order_status_set_doc_data($status, intval($ans->returnCode), strval($ans->description), intval($ans->docId), DT_ANAG);
        $rc = true;
    }
    else
    {
        //order_status_set_error($status, sprintf(__('Unknow error occurred while uploading order %d', 'fatt-24'), $orderId));
        $rc = false;
    }
    //store_order_status($orderId, $status);
    trace('Save Customer rc', $rc, $ans);
    
    return $rc;
}

/* 
 * Get Template List 
 */ 
function getTemplate($isOrder) 
{ 
    $res = api_call('GetTemplate', array());
    $listaNomi = array();
    $listaNomi['Predefinito'] = 'Predefinito';
    $xml = simplexml_load_string(utf8_encode($res));
    if (is_object($xml))
    {
        $listaModelli = $isOrder ? $xml->modelloOrdine : $xml->modelloFattura;
        foreach($listaModelli as $modello)
            $listaNomi[intval($modello->id)] = str_replace('\'', '\\\'', strval($modello->descrizione)) . " (ID: " . intval($modello->id) . ")";
    }
    else
        trace('error list templates', $res);
    return $listaNomi;
}

/*
 * Get Pdc List
 */
function getPdc()
{
    $res = api_call('GetPdc', array());
    $listaNomi = array();
    $listaNomi['Nessun Pdc'] = 'Nessun Pdc';
    $xml = simplexml_load_string(utf8_encode($res));
    if (is_object($xml))
    {
        foreach($xml->pdc as $pdc)
            if(intval($pdc->ultimoLivello) == 1)
                $listaNomi[intval($pdc->id)] = str_replace('^', '.', strval($pdc->codice)) . 
                    ' - ' . str_replace('\'', '\\\'', strval($pdc->descrizione)) . ' (ID: ' . intval($pdc->id) . ')';
    }
    else
        trace('error list pdc', $res);
    return $listaNomi;
}

/*
 * Get Sezionale List
 */
function getSezionale($idTipoDocumento)
{
    $res = api_call('GetNumerator', array());
    $listaNomi = array();
    $listaNomi['Predefinito'] = 'Predefinito';
    $xml = simplexml_load_string(utf8_encode($res));
    if (is_object($xml))
    {
        foreach($xml->sezionale as $sezionale)
            foreach($sezionale->doc as $doc)
                if(intval($doc->id) == $idTipoDocumento && intval($doc->stato) == 1)
                    $listaNomi[intval($sezionale->id)] = strval($sezionale->code) . " (ID: " . intval($sezionale->id) . ")";
    }
    else
        trace('error list sezionale', $res);
    return $listaNomi;
}

function getNatureColumn($tax_id)
{
	global $wpdb;
	$table_name = $wpdb->prefix . 'fattura_tax';
	$sql = "SELECT tax_code FROM $table_name WHERE tax_id =".$tax_id;
	$mycodeboj = $wpdb->get_row( $sql );
	return $mycodeboj->tax_code;
}