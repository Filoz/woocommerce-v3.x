<?php
/**
 * Fattura24.com
 * Description: handle basic settings
 * Author: Fattura24.com
 */
namespace fattura24;
if (!defined('ABSPATH'))
    exit;

if (is_admin())
{
	@session_start();
   
}

// display a specific settings page
function show_tax() {
    //page(__('Settings', 'fatt-24'));
  
	
	 global $wpdb;
        $table_name = $wpdb->prefix . "fattura_tax";
	
	
	$tax_id = $_POST["tax_id"];
    $tax_code = $_POST["tax_code"];
	$id=$_GET['id'];
    //insert
	$msg='';
    if (isset($_POST['insert'])) {
	   
	   if(empty($tax_id)){
		   $msg=__('Please select Tax Name','fatt-24');
	   }else if (empty($tax_code)){
		   $msg=__('Please enter Natura','fatt-24');
	   }else if (isset($_GET['id'])){
		   $row = $wpdb->get_results($wpdb->prepare("SELECT * from $table_name  where tax_id=%s AND id!=%s", $tax_id,$id));
			if(count($row)==0){
			$wpdb->update(
                $table_name, //table
               array('tax_id' => $tax_id, 'tax_code' => $tax_code), //data
                array('id' => $id), //where
                array('%s'), //data format
                array('%s','%s') //where format
			);
			$_SESSION['message']=__('Record has been updated successfully','fatt-24');
			wp_redirect('admin.php?page=fatt-24-tax');
			exit;
			}else{
				$msg=__('Tax code is already assign with the different Tax. please change the Tax','fatt-24');
			}
	   }else{
		    $row = $wpdb->get_results($wpdb->prepare("SELECT * from $table_name  where tax_id=%s", $tax_id));
			
			if(count($row)==0){
				$wpdb->insert(
					$table_name, //table
					array('tax_id' => $tax_id, 'tax_code' => $tax_code), //data
					array('%d', '%s') //data format			
				);
				//var_dump($wpdb->last_error);
				//exit;
				$_SESSION['message']=__('Record has been added successfully','fatt-24');
				wp_redirect('admin.php?page=fatt-24-tax');
				exit;
			}else{
				$msg=__('Duplicate entry found with the same Tax row','fatt-24');
			}
	   }
    }
	else if (isset($_GET['del'])) {
		$id=$_GET['del'];
        $wpdb->query($wpdb->prepare("DELETE FROM $table_name WHERE id = %s", $id));
		
		$_SESSION['message']=__('Record has been deleted successfully','fatt-24');
			wp_redirect('admin.php?page=fatt-24-tax');
			exit;
    }else if (isset($_GET['id'])) {
		
        $row = $wpdb->get_results($wpdb->prepare("SELECT * from $table_name  where id=%s", $id));
		if($row>0){
			$row=$row[0];
			$id=$row->id;
			$tax_code=$row->tax_code;
			$tax_id=$row->tax_id;
		}
    }
    ?>
	
	<div class='wrap'>
	<?php   page(__('Tax Configuration', 'fatt-24')); ?>
	
<?php if($msg){?>
<div class="notice notice-warning is-dismissible">
    <p><?php echo $msg ?></p>
</div>
<?php } ?>
<?php if(isset($_SESSION['message'])){?>
<div class="notice notice-success is-dismissible">
    <p><?php echo $_SESSION['message']; ?></p>
</div>
<?php unset($_SESSION['message']); } ?>

        <form method='post' >
		<table class='wp-list-table widefat fixed'>
			
                <tr>
                    <th class="ss-th-width" width="15%"><?php echo __('Tax','fatt-24'); ?></th>
                    <td>
					<?php 
					
		$sql="SELECT * from ".$wpdb->prefix."woocommerce_tax_rates where tax_rate_class = 'zero-rate' order by tax_rate_id desc";
	    $tax_rate = $wpdb->get_results($sql);
        
  $select = "<select name='tax_id' id='tax_id' class='postform'>";
  $select.= "<option value=''>".__('Select Tax','fatt-24')."</option>";
 
  foreach($tax_rate as $category){
		if($category->tax_rate_id==$tax_id)
			$select.= "<option value='".$category->tax_rate_id."' selected='selected'>".$category->tax_rate_name."</option>";
		else
			$select.= "<option value='".$category->tax_rate_id."'>".$category->tax_rate_name."</option>";
  }
 
  $select.= "</select>";
echo $select; 
 ?></td>
                </tr>
                <tr>
                    <th class="ss-th-width"><?php echo __('Natura','fatt-24'); ?></th>
                    <td><input type="text" name="tax_code" value="<?php echo $tax_code; ?>" class="ss-field-width" /></td>
                </tr>
				<tr>
                    <th class="ss-th-width"></th>
                    <td> <input type='submit' name="insert" value='Save' class='button'></td>
                </tr>
            </table>
			
           
		</form>
	</div>
    <div class='wrap'>
 
            <?php
        $sql="SELECT m.id,m.tax_id,m.tax_code,t.tax_rate_name from $table_name as m LEFT JOIN ".$wpdb->prefix."woocommerce_tax_rates as t ON (m.tax_id=t.tax_rate_id) order by m.id desc";
	   
        $rows = $wpdb->get_results($sql);
        ?>
        <table class='wp-list-table widefat fixed striped pages'>
			<thead>
            <tr>
                <th class="manage-column ss-list-width" width="80"><?php echo __('ID','fatt-24'); ?></th>
                <th class="manage-column ss-list-width"><?php echo __('Name','fatt-24'); ?></th>
                 <th class="manage-column ss-list-width"><?php echo __('Natura','fatt-24'); ?></th>
                <th colspan="2" width="150"><?php echo __('Action','fatt-24'); ?></th>
            </tr>
            </thead>
			<tbody>
			<?php foreach ($rows as $row) { ?>
                <tr>
                    <td class="manage-column ss-list-width"><?php echo $row->id; ?></td>
                    <td class="manage-column ss-list-width"><?php echo $row->tax_rate_name; ?></td>
                    <td class="manage-column ss-list-width"><?php echo $row->tax_code; ?></td>
                    <td><a href="<?php echo admin_url('admin.php?page=fatt-24-tax&id=' . $row->id); ?>"><?php echo __('Edit','fatt-24'); ?></a></td>
					<td><a href="<?php echo admin_url('admin.php?page=fatt-24-tax&del=' . $row->id); ?>"><?php echo __('Delete','fatt-24'); ?></a></td>
                </tr>
            <?php } ?>
			</tbody>
        </table>
     
    </div>
    
    <?php
}