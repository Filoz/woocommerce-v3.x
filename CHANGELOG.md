# Changelog

## 3.5.3
###### _Dec 11, 2018_
- modificato label 'Codice recipiente' in 'Codice destinatario'
- corretta la valorizzazione automatica di 'Codice destinatario' con '0000000'

## 3.5.2
###### _Dec 11, 2018_
- compatibile con fatturazione elettronica

## 3.4.2
###### _Ott 30, 2018_
- compatibile con multishop woocommerce

## 3.3.2
###### _Apr 06, 2018_
- piccola modifica sui dati dell'ordine inviati a Fattura24 dall'e-commerce

## 3.3.1
###### _Mar 28, 2018_
- aggiunto codice del coupon alla descrizione della relativa voce in fattura

## 3.3.0
###### _Mar 06, 2018_
- aggiunta sezione di Log nella configurazione del modulo in cui è possibile scaricare il file di log di Fattura24 o cancellarlo dal proprio server

## 3.2.5
###### _Feb 28, 2018_
- cambiata la descrizione in fattura della riga dello sconto

## 3.2.4
###### _Feb 08, 2018_
- risolto bug su iva errata dei prodotti
- abilitato l’invio a Fattura24 del nome della tassa applicata al prodotto
- il campo “Oggetto” della ricevuta/fattura in Fattura24 ora viene valorizzato con il numero dell’ordine in WooCommerce

## 3.2.2
###### _Nov 16, 2017_
- aggiunta selezione sezionale per ricevute e fatture. Ora è possibile scegliere il sezionale dei documenti creati dagli e-commerce, cosicché si possono usare sezionali diversi per diversi e-commerce

## 3.2.1
###### _Nov 10, 2017_
- aggiunta selezione modello PDF per ordini e fatture con destinazione. Ora è possibile scegliere modelli diversi per i documenti a seconda che l'ordine contenga o meno l'indirizzo di spedizione del cliente
- aggiunta selezione conto. Ora è possibile scegliere il conto economico da associare alle prestazioni/prodotto dei documenti generati dall'e-commerce

## 3.2.0
###### _Ott 12, 2017_
- risolto bug su creazione ordine da parte di un utente non registrato

## 3.1.2
###### _Set 12, 2017_
- aggiunti alcuni messaggi di avviso nella pagina di configurazione del modulo
- rimossa checkbox 'Scarica PDF' relativa ai PDF delle fatture nella pagina di configurazione del modulo. Ora i PDF vengono sempre scaricati sul proprio e-commerce quando viene creata una fattura

## 3.1.1
###### _Set 01, 2017_
- risolto bug che causava un errore durante l'installazione se la versione di PHP è minore della 5.5

## 3.1.0
###### _Ago 24, 2017_
- spostati i campi 'Codice Fiscale' e 'Partita IVA' nella pagina di Checkout dell'ordine. Ora sono nella sezione 'Dettagli di fatturazione'

## 3.0.9
###### _Ago 02, 2017_
- aggiunta gestione magazzino nella pagina di configurazione del modulo. Ora è possibile decidere se gli ordini e/o le fatture movimentano il magazzino in Fattura24
- aggiunta configurazione per i modelli di ordini e fatture. Ora è possibile scegliere con quale modello vengono creati i PDF degli ordini e delle fatture

## 3.0.8
###### _Giu 28, 2017_
- aggiunta funzionalità per cui l'ordine impegna la merce nel magazzino di Fattura24 e la fattura la scarica se il prodotto ha lo stesso codice che ha sull'e-commerce
- ora i dati dell'ordine vengono inviati a Fattura24 anche se questo viene creato, o editato (se i suoi dati non sono mai stati inviati precedentemente), dal pannello di amministrazione

## 3.0.7
###### _Giu 20, 2017_
- aggiunto avviso ‘Nuova versione disponibile’ nella pagina di configurazione del plugin
- risolto bug minore

## 3.0.6
###### _Giu 12, 2017_
- risolto bug minore

## 3.0.5
###### _Giu 5, 2017_
- risolto bug minore

## 3.0.4
###### _Mag 24, 2017_
- risolto bug per cui non veniva creato l’ordine in Fattura24 quando il pagamento era effettuato tramite Paypal

## 3.0.3
###### _Mag 17, 2017_
- risolto bug su invio Iva errata

## 3.0.2
###### _Mag 16, 2017_
- risolto bug che si verificava in presenza di prodotti scontati tramite certi plugin di WordPress

## 3.0.1
###### _Mag 11, 2017_
- risolti bug minori
